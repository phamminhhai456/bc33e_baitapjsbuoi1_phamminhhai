/**
 * Bài 1 : Tính tiền lương nhân viên
 * Input :
 * Số ngày làm , tiền lương 1 ngày : 100.000
 *
 * Step:
 * s1: Tạo biến số ngày làm , biến tiền lương 1 ngày
 * s2 : Tạo biến tiền lương = số ngày làm * tiền lương 1 ngày
 *
 * Output:
 * Tiền lương nhân viên
 */

var soNgayLam = 26;
const luongMotNgay = 100000;
var luongNhanVien = soNgayLam * luongMotNgay;
console.log(
  "Lương nhân viên : ",
  new Intl.NumberFormat("de-DE").format(luongNhanVien)
);

/**
 * Bài 2: Tính giá trị trung bình
 * Input:
 * 5 số thực
 *
 * Step:
 * s1: Tạo 5 biến số thực
 * s2: Tạo biến trung bình = tổng 5 số thực / 5
 *
 * Output:
 * Giá trị trung bình của 5 số
 */

var num1 = 4;
var num2 = 5;
var num3 = 8;
var num4 = 9;
var num5 = 10;

var average = (num1 + num2 + num3 + num4 + num5) / 5;

console.log("Số trung bình: ", average);

/**
 * Bài 3: Quy đổi tiền
 * Input:
 * Số tiền USD
 *
 * Step:
 * s1:Tạo biến số tiền USD, biến VN = 23.500
 * s2: Tạo biến kết quả = tiền USD * tiền VN
 *
 * Output:
 * Số tiền được quy đổi
 */

var USD = 2;
const VN = 23500;

var result = USD * VN;
console.log(
  "Tiền quy đổi từ USD sang VND : ",
  new Intl.NumberFormat("de-DE").format(result)
);

/**
 * Bài 4:
 *
 * Input:
 * Chiều dài, chiều rộng hình chữ nhật
 *
 * Step:
 * s1: Tạo biến chiều dài, chiều rộng hình chữ nhật
 * s2: Tạo biến diện tích hcn = chiều dài * chiều rộng, biến chu vi = (chiều dài + chiều rộng) *2
 *
 * Output:
 * Diện tích hcn, Chu vi hcn
 */

var chieuDai = 8;
var chieuRong = 5;

var dienTich = chieuDai * chieuRong;

var chuVi = (chieuDai + chieuRong) * 2;

console.log("Diện tích: ", dienTich);
console.log("Chu vi: ", chuVi);

/**
 * Bài 5:
 *
 * Tính tổng 2 ký số
 *
 * Input : Số có 2 chữ số
 *
 * Step :
 * s1: Tạo biến số có 2 chữ số
 * s2:
 * Tạo biến số hàng đơn vị = số % 10;
 * Tạo biến số hàng chục = số / 10;
 * s3:
 * Tạo biến tổng = số hàng đơn vị  + số hàng chục
 * Sử dụng hàm Math.floor() làm tròn giá trị hàng chục
 *
 * Output:
 * Tổng 2 ký số của số vừa nhập
 */

var num = 87;

var unit = num % 10;
var dozen = num / 10;

var sum = unit + Math.floor(dozen);

console.log("Tổng 2 ký số: ", sum);
